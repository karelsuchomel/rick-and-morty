# Rick and Morty
## Description
Simple application in React/Redux from scratch(without CRA) using rick-and-morty api. Still in progress!! 
## Currently "working" features:
1. Search a character by a name
2. Open a single character by clicking on it's name or picture (not fully)
3. Fetching all characters by pages (displaying only first page of a response)

## Usage
1. Install all dependecies with npm i
2. Enter npm run start into console to start webpack-dev-server
3. New tab will automaticly open in a browser, or on the port 4000

