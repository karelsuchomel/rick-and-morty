import _ from 'lodash';
import { FETCH_FILTER } from '../actions/types';

export default (state = {}, action) => {
    switch (action.type) {
        case FETCH_FILTER:
            return { ..._.mapKeys(action.payload, 'id') };
        default:
            return state;
    }
};
