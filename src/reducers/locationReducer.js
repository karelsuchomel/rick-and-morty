import _ from 'lodash';
import { FETCH_LOCATIONS, FETCH_LOCATION } from '../actions/types';

export default (state = {}, action) => {
    switch (action.type) {
        case FETCH_LOCATIONS:
            return { ...state, ..._.mapKeys(action.payload, 'id') };
        case FETCH_LOCATION:
            return { ...state, [action.payload.id]: action.payload };
        default:
            return state;
    }
};
