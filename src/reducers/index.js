import { combineReducers } from 'redux';
import charactersReducer from './characterReducer';
import locationReducer from './locationReducer';
import { reducer as formReducer } from 'redux-form';
import filterReducer from './filterReducer';
import infoReducer from './infoReducer';

export default combineReducers({
    characters: charactersReducer,
    locations: locationReducer,
    form: formReducer,
    filtered: filterReducer,
    info: infoReducer
});
