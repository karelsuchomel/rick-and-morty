import _ from 'lodash';
import { FETCH_CHARACTERS, FETCH_CHARACTER, FETCH_INFO } from '../actions/types';

export default (state = {}, action) => {
    switch (action.type) {
        case FETCH_CHARACTERS:
            return { ..._.mapKeys(action.payload, 'id') };
        case FETCH_CHARACTER:
            return { ...state, [action.payload.id]: action.payload };
        case FETCH_INFO:
            return { ...state, info: action.payload.info };
        default:
            return state;
    }
};
