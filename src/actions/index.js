import characters from '../api/characters';

import {
    FETCH_CHARACTERS,
    FETCH_CHARACTER,
    FETCH_FILTER,
    FETCH_INFO
} from './types';

export const fetchCharacters = page => async dispatch => {
    const response = await characters.get(page);

    //console.log(response.data);
    dispatch({
        type: FETCH_CHARACTERS,
        payload: response.data.results
    });
    dispatch({
        type: FETCH_INFO,
        payload: response.data.info
    });
    //console.log(response.data);
};

export const fetchCharacter = id => async dispatch => {
    const response = await characters.get(`/character/${id}`);

    dispatch({
        type: FETCH_CHARACTER,
        payload: response.data
    });
};

export const fetchFilteredCharacters = term => async dispatch => {
    const response = await characters.get(`/character/?name=${term}`);
    dispatch({
        type: FETCH_FILTER,
        payload: response.data.results
    });
};
