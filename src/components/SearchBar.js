import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { fetchFilteredCharacters } from '../actions';

class SearchBar extends Component {
    renderInput = ({ input, meta }) => {
        return (
            <div className='field'>
                <input {...input} autoComplete='off' placeholder='Enter a name' />
                {this.renderError(meta)}
            </div>
        );
    };

    renderError({ error, touched }) {
        if (touched && error) {
            return (
                <div className='message'>
                    <div className='header'>{error}</div>
                </div>
            );
        }
    }

    onFormSubmit = formValues => {
        this.props.fetchFilteredCharacters(formValues.name);
        this.props.reset();
    };

    render() {
        return (
            <form onSubmit={this.props.handleSubmit(this.onFormSubmit)}>
                <Field name='name' component={this.renderInput} />
            </form>
        );
    }
}

const validate = formValues => {
    const errors = {};

    if (!formValues.name) {
        errors.name = 'You must enter a name';
    }

    return errors;
};

export default connect(null, { fetchFilteredCharacters })(
    reduxForm({
        form: 'SearchBar',
        validate
    })(SearchBar)
);
