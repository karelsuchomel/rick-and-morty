import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchCharacters } from '../actions';
import CharacterCard from './CharacterCard';

class CharactersList extends Component {
    constructor(props) {
        super(props);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.onButtonClick = this.onButtonClick.bind(this);
        this.state = { page: 'https://rickandmortyapi.com/api/character/' };
    }
    componentDidMount() {
        this.props.fetchCharacters(this.state.page);
    }

    renderList() {
        return this.props.characters.map(character => {
            if (character) {
                return <CharacterCard character={character} key={character.id} />;
            }
        });
    }

    onFormSubmit(e) {
        e.preventDefault();
        this.props.fetchCharacters(this.state.page);
        window.scrollTo(0, 0);
    }

    onButtonClick(e) {
        this.setState({ page: e.target.value });
    }
    renderButtons() {
        if (this.props.info) {
            if (!this.props.info.prev) {
                return (
                    <button
                        type='submit'
                        value={this.props.info.next}
                        onClick={this.onButtonClick}
                        className='btn'
                    >
                        NEXT
                    </button>
                );
            }
            return (
                <React.Fragment>
                    <button
                        type='submit'
                        value={this.props.info.prev}
                        onClick={this.onButtonClick}
                        className='btn'
                    >
                        PREVIOUS
                    </button>
                    <button
                        type='submit'
                        value={this.props.info.next}
                        onClick={this.onButtonClick}
                        className='btn'
                    >
                        NEXT
                    </button>
                </React.Fragment>
            );
        }
    }

    render() {
        if (!this.props.characters) {
            return <div>Loading...</div>;
        }
        return (
            <div className='container'>
                <div className='cards-container'>{this.renderList()}</div>
                <form onSubmit={this.onFormSubmit} className='pagination'>
                    {this.renderButtons()}
                </form>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        characters: Object.values(state.characters),
        info: state.info.info
    };
};

export default connect(mapStateToProps, { fetchCharacters })(CharactersList);
