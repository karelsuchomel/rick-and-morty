import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import history from '../history';

//Components
import Header from './Header';
import CharactersList from './CharactersList';
import Character from './Character';
import SearchBar from './SearchBar';
import Homepage from './Homepage';

const App = () => {
    return (
        <React.Fragment>
            <Router history={history}>
                <Header />
                <div>
                    <Switch>
                        <Route path='/' exact component={Homepage}></Route>
                        <Route
                            path='/characters/'
                            exact
                            component={CharactersList}
                        ></Route>
                        <Route
                            path='/characters/:id'
                            exact
                            component={Character}
                        ></Route>
                    </Switch>
                </div>
            </Router>
        </React.Fragment>
    );
};

export default App;
