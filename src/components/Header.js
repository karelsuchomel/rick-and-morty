import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import { withRouter } from 'react-router';

const Header = () => {
    return (
        <div className='navigation'>
            <Link to='/' className='logo'>
                Rick And Morty
            </Link>
            <div className='right-nav'>
                <NavLink to='/' className='item' exact activeClassName='active'>
                    Home
                </NavLink>
                <NavLink to='/characters/' className='item' exact>
                    All Characters
                </NavLink>
            </div>
        </div>
    );
};

export default withRouter(Header);
