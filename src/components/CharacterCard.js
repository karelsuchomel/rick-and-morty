import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class CharacterCard extends Component {
    render() {
        const { character } = this.props;
        let status = 'unknown';
        if (character.status == 'Alive') {
            status = 'alive';
        } else if (character.status == 'Dead') {
            status = 'dead';
        }

        if (!this.props.character) {
            return <div>Loading...</div>;
        }

        return (
            <div className='card'>
                <Link to={`/characters/${character.id}`} className='image'>
                    <img
                        src={character.image}
                        alt={`picture of ${character.name}`}
                    />
                </Link>
                <div className='content'>
                    <Link to={`/characters/${character.id}`} className='title'>
                        {character.name}
                    </Link>
                    <span className='status'>
                        <span className={status}>{character.status}</span> -{' '}
                        {character.species}
                    </span>
                    <p>Last known location: </p>
                    <Link
                        to={`/location/${character.location.name}`}
                        className='location'
                    >
                        {character.location.name}
                    </Link>
                </div>
            </div>
        );
    }
}

export default CharacterCard;
