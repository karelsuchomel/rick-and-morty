import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchCharacter } from '../actions';
import { Link } from 'react-router-dom';

class Character extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        const { id } = this.props.match.params;
        this.props.fetchCharacter(id);
    }

    renderList() {
        const { episode } = this.props.character;

        return this.props.character.episode.map(episode => {
            return (
                <li key={episode}>
                    <a href={episode}>{episode}</a>
                </li>
            );
        });
    }

    render() {
        const { character } = this.props;
        let status = 'unknown';

        if (!character) {
            return <div>Loading...</div>;
        } else {
            if (character.status == 'Alive') {
                status = 'alive';
            } else if (character.status == 'Dead') {
                status = 'dead';
            }
            return (
                <div className='container'>
                    <div className='cards-container'>
                        <div className='card'>
                            <div className='image'>
                                <img
                                    src={character.image}
                                    alt={`picture of ${character.name}`}
                                />
                            </div>
                            <div className='content'>
                                <Link
                                    to={`/characters/${character.id}`}
                                    className='title'
                                >
                                    {character.name}
                                </Link>
                                <span className='status'>
                                    <span className={status}>
                                        {character.status}
                                    </span>{' '}
                                    - {character.species}
                                </span>
                                <p>Last known location: </p>
                                <Link
                                    to={`/location/${character.location.name}`}
                                    className='location'
                                >
                                    {character.location.name}
                                </Link>
                            </div>
                        </div>
                        <div className='episodes'>
                            <ul>
                                Episodes
                                {this.renderList()}
                            </ul>
                        </div>
                    </div>
                </div>
            );
        }

        console.log(character);
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        character: state.characters[ownProps.match.params.id]
    };
};

export default connect(mapStateToProps, { fetchCharacter })(Character);
