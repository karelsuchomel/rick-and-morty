import React, { Component } from 'react';
import { connect } from 'react-redux';
import CharacterCard from './CharacterCard';
import SearchBar from './SearchBar';

class Homepage extends Component {
    renderList() {
        return this.props.characters.map(character => {
            return <CharacterCard character={character} key={character.id} />;
        });
    }

    render() {
        return (
            <React.Fragment>
                <SearchBar />
                <div className='container'>
                    <div className='cards-container'>{this.renderList()}</div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        characters: Object.values(state.filtered)
    };
};

export default connect(mapStateToProps)(Homepage);
