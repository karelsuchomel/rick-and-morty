import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchFilteredCharacters } from '../actions';
import CharacterCard from './CharacterCard';

class SearchResult extends Component {
    componentDidMount() {
        this.props.fetchFilteredCharacters(this.props.term);
    }

    renderList() {
        return this.props.characters.map(character => {
            return <CharacterCard character={character} key={character.id} />;
        });
    }

    render() {
        if (!this.props.characters) {
            return <div>Loading...</div>;
        }
        return (
            <div className='container'>
                <div className='cards-container'>{this.renderList()}</div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        characters: Object.values(state.characters),
        term: state.SearchBar.values.name
    };
};

export default connect(mapStateToProps, { fetchFilteredCharacters })(SearchResult);
